$(document).ready(function() {
    var text_max = 50;
    $('#textarea_feedback').html(text_max + ' tähemärki jäänud');

    $('#selgitus').keyup(function() {
        var text_length = $('#selgitus').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' tähemärki jäänud');
    });
});

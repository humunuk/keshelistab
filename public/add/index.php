<?php
error_reporting(-1);
require '../../vendor/autoload.php'; // Require autoload


$data = new \Telmarket\DBAction(); // Define class names
$data->connect(); // Get connection to database

// Add base template for page
$template =  new \Telmarket\Template("../../views/base.phtml");

// Add specific content to current page with passed variables
$template->render("../../views/add/index.phtml", ['data'=>$data]);
?>

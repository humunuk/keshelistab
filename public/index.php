<?php
error_reporting(-1);
require '../vendor/autoload.php'; // Require autoload


$data = new \Telmarket\DBAction(); // Define class names
$data->connect(); // Get connection to database

$pageAction = new \Telmarket\PageAction();

// Get all numbers
$numbrid = $data->getAllNumbers();

// Add base template for page
$template =  new \Telmarket\Template("../views/base.phtml");

// Add specific content to current page with passed variables
$template->render("../views/index/index.phtml", ['numbrid'=>$numbrid, 'data'=>$data, 'pageAction'=>$pageAction]);
?>

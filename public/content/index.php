<?php
error_reporting(-1);
require '../../vendor/autoload.php'; // Require autoload


$data = new \Telmarket\DBAction(); // Define class names
$data->connect(); // Get connection to database

$items = new \Telmarket\PageAction();

$id = htmlspecialchars($_GET['id']);

// Get all numbers
$nr_content = $data->displayNumberContent($id)->fetch();
$li_komms = $data->displayAllComments($id)->fetchAll(PDO::FETCH_ASSOC);

// Add base template for page
$template =  new \Telmarket\Template("../../views/base.phtml");

// Add specific content to current page with passed variables
$template->render(
          "../../views/content/index.phtml",
          [
          'items'=>$items,
          'id'=>$id,
          'nr_content'=>$nr_content,
          'li_komms'=>$li_komms,
          'data'=>$data
          ]
          );
?>

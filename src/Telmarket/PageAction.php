<?php

namespace Telmarket;

class PageAction {

  public function getItemsSubset($allListItems, $positionStart, $positionEnd) { // Returns all numbers from db,
    // but for range given.

    $subset = array();

    $position = 0;
    foreach($allListItems as $list) {
      $position += 1;
      if ($position >= $positionStart && $position <= $positionEnd) {
        $subset[] = $list;
      }
    }
    return $subset;

  }

  public function getPages($total_pages, $current_page, $id) { // Pagination for pages

    $i = 0;
    while ($i < $total_pages) {
      $i += 1;
      if ($i == $current_page) {
        echo "<span> $i </span>";
      } else {
        echo "<a href=./index.php?id=$id&pg=$i> $i </a>";
      }
    }
  }

  public function getPagesList($total_pages, $current_page) { // Pagination for pages

    $i = 0;
    while ($i < $total_pages) {
      $i += 1;
      if ($i == $current_page) {
        echo "<span> $i </span>";
      } else {
        echo "<a href=./index.php?pg=$i class='pagination-lg'> $i </a>";
      }
    }
  }

  public function itemsPosition($current_page, $total_items, $numbrid) {

    $start = (($current_page - 1) * $total_items) + 1;
    $end = $current_page * $total_items;

    if ($end > count($numbrid)) {
      $end = count($numbrid);
    }

    return array('start'=>$start, 'end'=>$end);


  }

}

<?php
namespace Telmarket;

error_reporting(-1);

// Learn to deal with classes in php

class DBAction {  // Define a class

  // Define properties for the class, they can be public, private or protected
  // public is default

  protected $db;
  public $id;

  // Define methods for the class, they can be public, private or protected
  // public is default


  public function connect()
  {  // Function to connect with MySQL database
    $this->db = new \PDO('mysql:host=localhost;dbname=test', "", "");
    $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
  }

  public function getAllNumbers() { // Function to fetch all numbers from database

    $results = $this->db->prepare('SELECT telnr, selgitus FROM telmyyjad ORDER BY jrk DESC');
    $results->execute();

    return $results->fetchAll(\PDO::FETCH_ASSOC);

  }

  public function searchAllNumbers() { // Function to return database search by number

    $data = htmlspecialchars(str_replace(" ", "", "%".$_GET['s']."%")); // removes whitespaces
    $otsi = 'SELECT * FROM telmyyjad WHERE telnr LIKE ?'; // info for query
    $valmis = $this->db->prepare($otsi);
    $valmis->execute(array($data));
    $telnrid = $valmis->fetchAll(\PDO::FETCH_ASSOC);

    return $telnrid;
  }

  public function displayNumberContent($id) {  // Display number+initial comment for current dynamic page

    $nr_content = $this->db->prepare('SELECT telnr, selgitus FROM telmyyjad WHERE telnr=:id');
    $nr_content->bindParam(":id", $id, \PDO::PARAM_INT);
    $nr_content->execute();

    return $nr_content;


  }

  public function displayAllComments($id) {  // Display comments for selected number on dynamic page

    $li_komms = $this->db->prepare('SELECT telnr, komm FROM tel_komm WHERE telnr=:id ORDER BY jrk DESC');
    $li_komms->bindParam(":id", $id, \PDO::PARAM_INT);
    $li_komms->execute();

    return $li_komms;

  }

  public function addComment($puhas_nr_com, $puhas_komm) { // Add additional comment to number page


    $komm_p2ring = "INSERT INTO tel_komm (telnr, komm) VALUES (:nrcom, :komm)";
    $komm_sql = $this->db->prepare($komm_p2ring);
    $komm_sql->bindParam(':nrcom', $puhas_nr_com, \PDO::PARAM_INT);
    $komm_sql->bindParam(':komm', $puhas_komm, \PDO::PARAM_STR);
    $komm_sql->execute();


  }

  public function addNumber($nmbr_clean, $selgitus) {

    $nr_p2ring = "INSERT INTO telmyyjad (telnr, selgitus) VALUES (:pnumber, :selgitus)";
    $nr_sql = $this->db->prepare($nr_p2ring);
    $nr_sql->bindParam(':pnumber', $nmbr_clean, \PDO::PARAM_INT);
    $nr_sql->bindParam(':selgitus', $selgitus, \PDO::PARAM_STR);
    $nr_sql->execute();

  }

}


?>

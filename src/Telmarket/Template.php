<?php
namespace Telmarket;

class Template {  // Name of a class to generate content to each page.
  protected $base_template;
  protected $page;

  public function __construct($base_template)
  {
    $this->base_template = $base_template;
  }

  // @page /path/to/TemplateFile in views folder
  // @data is predefined variable(s) in a view file /path/to/public folder for current page
  //          passed as array to render() function

  public function render($page, $data = array())
  {
    foreach ($data as $key => $value) {
      $this->{$key} = $value;
    }

    $this->page = $page;
    require $this->base_template;
  }

  public function content()
  {
    require $this->page;
  }
}


?>
